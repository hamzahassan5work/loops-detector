﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Loops_Test
{
    public partial class Form1 : Form
    {     
        private int forcount, whilecount, dowhilecount = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog Opendialog = new OpenFileDialog();
            Opendialog.Filter = "Text Files (*.txt)|*.txt| Rich Text Format (*.rtf)|*.rtf| C++ Source Files (*.cpp)|*.cpp";

            DialogResult result = Opendialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                if (Path.GetExtension(Opendialog.FileName) == ".txt" || Path.GetExtension(Opendialog.FileName) == ".rtf" || Path.GetExtension(Opendialog.FileName) == ".cpp")
                    pathbox.Text = Path.GetFullPath(Opendialog.FileName);
                   
                else
                    MessageBox.Show("Please Select Text File Only", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pathbox_TextChanged(object sender, EventArgs e)
        {
            calculatebtn.Enabled = true;
            forloopbox.Text = "";
            whileloopbox.Text = "";
            dowhileloopbox.Text = "";
        }

        private void calculatebtn_Click(object sender, EventArgs e)
        {
            string path = pathbox.Text;
            

            forloopfunction(path);

            whileloopfunction(path);

         // dowhileloopfunction(path);

            forloopbox.Text = forcount.ToString();
            whileloopbox.Text = whilecount.ToString();
            dowhileloopbox.Text = dowhilecount.ToString();

        }

        private int forloopfunction(string path)
        {
            StreamReader sr= new StreamReader(path);
            while (!sr.EndOfStream)
            {
                string text = sr.ReadLine();
                string[] lines = text.Split(';');

                foreach (string line in lines)
                {
                    string[] subline = line.Split('=');

                    foreach (string sbl in subline)
                    {
                        if (sbl.Contains("\"") != true)
                        {
                            if (sbl.Contains("for(") || sbl.Contains("for ("))
                            {
                                forcount += 1;
                            }
                        }
                    }
                }
            }

            return forcount;
        }

        private int whileloopfunction(string path)
        {
            StreamReader sr2 = new StreamReader(path);
            while (!sr2.EndOfStream)
            {
                string text = sr2.ReadLine();
                string[] lines = text.Split(';');

                foreach (string line in lines)
                {
                    if (line.Contains("while(") || line.Contains("while ("))
                    {
                        string[] subline = line.Split('(');
                        foreach (string sbl in subline)
                        {
                            if (sbl.Contains("do") != true)
                            {
                                if (sbl.Contains("\"") != true)
                                {
                                    whilecount += 1;
                                }
                            }
                        }
                    }
                }
            }

            return whilecount;
        }

        private int dowhileloopfunction(string path)
        {
            StreamReader sr3 = new StreamReader(path);
            while (!sr3.EndOfStream)
            {
                string text = sr3.ReadLine();
                string[] lines = text.Split(';');

                foreach (string line in lines)
                {
                    if (line.Contains("while(") || line.Contains("while ("))
                    {
                        string[] subline = line.Split('(');
                        foreach (string sbl in subline)
                        {
                            if (sbl.Contains("do") == true)
                            {
                                if (sbl.Contains("\"") != true)
                                {
                                    dowhilecount += 1;
                                }
                            }
                        }
                    }
                }
            }

            return dowhilecount;
        }

    }
}
