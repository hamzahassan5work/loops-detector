﻿namespace Loops_Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.pathbox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.calculatebtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dowhileloopbox = new System.Windows.Forms.Label();
            this.whileloopbox = new System.Windows.Forms.Label();
            this.forloopbox = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(38, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Path:";
            // 
            // pathbox
            // 
            this.pathbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pathbox.Location = new System.Drawing.Point(87, 19);
            this.pathbox.Name = "pathbox";
            this.pathbox.ReadOnly = true;
            this.pathbox.Size = new System.Drawing.Size(438, 22);
            this.pathbox.TabIndex = 1;
            this.pathbox.TabStop = false;
            this.pathbox.TextChanged += new System.EventHandler(this.pathbox_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(546, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Select File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // calculatebtn
            // 
            this.calculatebtn.BackColor = System.Drawing.Color.FloralWhite;
            this.calculatebtn.Enabled = false;
            this.calculatebtn.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.calculatebtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calculatebtn.ForeColor = System.Drawing.SystemColors.ControlText;
            this.calculatebtn.Location = new System.Drawing.Point(281, 64);
            this.calculatebtn.Name = "calculatebtn";
            this.calculatebtn.Size = new System.Drawing.Size(97, 30);
            this.calculatebtn.TabIndex = 0;
            this.calculatebtn.Text = "Calculate";
            this.calculatebtn.UseVisualStyleBackColor = false;
            this.calculatebtn.Click += new System.EventHandler(this.calculatebtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dowhileloopbox);
            this.groupBox1.Controls.Add(this.whileloopbox);
            this.groupBox1.Controls.Add(this.forloopbox);
            this.groupBox1.Location = new System.Drawing.Point(12, 121);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(634, 168);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(468, 2);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(121, 43);
            this.label6.TabIndex = 1;
            this.label6.Text = "Total Number Of   DO-WHLE Loops:";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(261, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 43);
            this.label4.TabIndex = 2;
            this.label4.Text = "Total Number Of    WHILE Loops:";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(54, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 43);
            this.label2.TabIndex = 3;
            this.label2.Text = "Total Number Of     FOR Loops:";
            // 
            // dowhileloopbox
            // 
            this.dowhileloopbox.BackColor = System.Drawing.SystemColors.Control;
            this.dowhileloopbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dowhileloopbox.Font = new System.Drawing.Font("Arial", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dowhileloopbox.Location = new System.Drawing.Point(472, 53);
            this.dowhileloopbox.Name = "dowhileloopbox";
            this.dowhileloopbox.Size = new System.Drawing.Size(108, 87);
            this.dowhileloopbox.TabIndex = 4;
            this.dowhileloopbox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // whileloopbox
            // 
            this.whileloopbox.BackColor = System.Drawing.SystemColors.Control;
            this.whileloopbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.whileloopbox.Font = new System.Drawing.Font("Arial", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.whileloopbox.Location = new System.Drawing.Point(265, 53);
            this.whileloopbox.Name = "whileloopbox";
            this.whileloopbox.Size = new System.Drawing.Size(108, 87);
            this.whileloopbox.TabIndex = 5;
            this.whileloopbox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // forloopbox
            // 
            this.forloopbox.BackColor = System.Drawing.SystemColors.Control;
            this.forloopbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.forloopbox.Font = new System.Drawing.Font("Arial", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.forloopbox.Location = new System.Drawing.Point(56, 53);
            this.forloopbox.Name = "forloopbox";
            this.forloopbox.Size = new System.Drawing.Size(108, 87);
            this.forloopbox.TabIndex = 6;
            this.forloopbox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(658, 308);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.calculatebtn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pathbox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Loop Detector";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox pathbox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button calculatebtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label dowhileloopbox;
        private System.Windows.Forms.Label whileloopbox;
        private System.Windows.Forms.Label forloopbox;
    }
}

